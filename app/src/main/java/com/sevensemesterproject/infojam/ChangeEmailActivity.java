package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.ChangeEmailRequest;
import com.sevensemesterproject.infojam.dto.EditEmailResponse;
import com.sevensemesterproject.infojam.utils.CheckNetworkStatus;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.Validator;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.util.HashMap;
import java.util.Map;

public class ChangeEmailActivity extends AppCompatActivity {

    private EditText olde,newe,confirme;
    private Button changeemailbtn;
    private String BASE_URL;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);

        BASE_URL = getString(R.string.base_url);
        //

        olde = findViewById(R.id.olde);
        newe = findViewById(R.id.newe);
        confirme = findViewById(R.id.confirme);
        changeemailbtn = findViewById(R.id.change_email_button);
        changeemailbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sets data for changing email only after validation
                if(validate()){
                    ChangeEmailRequest changeEmailRequest = new ChangeEmailRequest();
                    changeEmailRequest.setOldEmail(olde.getText().toString());
                    changeEmailRequest.setNewEmail(newe.getText().toString());
                    changeEmailRequest.setConfirmNewEmail(confirme.getText().toString());
                    changeEmail(changeEmailRequest);
                }
            }
        });
    }

    /**
     * validates data for changing email
     * @return boolean
     */
    private boolean validate() {
        String conNewEmail = confirme.getText().toString().trim();
        String newEmail = newe.getText().toString().trim();
        String oldEmail = olde.getText().toString().trim();
        Validator validator = new Validator();
        //check if old email entered is empty
        if(oldEmail.isEmpty()){
            validator.warnField(olde, "Enter your old Email!");
            return false;
        }
        //check if new email entered is empty
        if (newEmail.isEmpty()) {
            validator.warnField(newe, "Enter a new Email!");
            return false;
        }
        //check if new email entered is valid
        if(!validator.validateEmailPattern(newEmail)){
            validator.warnField(newe, "Invalid Email!");
            return false;
        }
        //check if confirmed new email entered is empty
        if (conNewEmail.isEmpty()){
            validator.warnField(confirme,"Confirm new Email!");
            return false;
        }
        //check if new email and confirmed new email is same
        if(!validator.matchTwoEmails(conNewEmail, newEmail)){
            validator.warnField(newe, "Email didn't match!");
            validator.warnField(confirme,"Email didn't match!");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ChangeEmailActivity.this, SettingsActivity.class));
        finish();
    }

    /**
     * creates email changing request
     * @param changeEmailRequest
     */
    private void changeEmail(ChangeEmailRequest changeEmailRequest) {
        String url = BASE_URL + getString(R.string.change_email_url);

        Gson gson = new GsonBuilder().create();
        final String data = gson.toJson(changeEmailRequest);

        StringRequest putRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Edit Response", response);
                EditEmailResponse emailResponse = new GsonBuilder().create().fromJson(response, EditEmailResponse.class);
                Toast.makeText(ChangeEmailActivity.this, "Email Updated!", Toast.LENGTH_SHORT).show();
                SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = userPref.edit();
                editor.putString(getString(R.string.email), emailResponse.getEmail());
                editor.apply();
                startActivity(new Intent(ChangeEmailActivity.this, SettingsActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return data.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", String.valueOf(new SharedPreferencesUtil().getLoginId(ChangeEmailActivity.this)));
                params.put("token", new SharedPreferencesUtil().getToken(ChangeEmailActivity.this));
                return params;
            }

            public String getBodyContentType() {
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(putRequest);
    }
}
