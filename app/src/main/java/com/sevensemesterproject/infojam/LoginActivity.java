package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.LoginResponse;
import com.sevensemesterproject.infojam.dto.UserLoginRequest;
import com.sevensemesterproject.infojam.utils.Validator;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

public class LoginActivity extends AppCompatActivity {
    private EditText email,password;
    private Button loginBtn;
    private TextView signupbtn;
    private String BASE_URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BASE_URL = getString(R.string.base_url);

        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);
        signupbtn = findViewById(R.id.login_signup);
        loginBtn = findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isValid = validateLogin();
                //data is loaded to request model only after validation is true
                if(isValid){
                    UserLoginRequest userLoginRequest = new UserLoginRequest();
                    userLoginRequest.setEmail(email.getText().toString().trim());
                    userLoginRequest.setPassword(password.getText().toString().trim());
                    userLogin(userLoginRequest);
                }
            }
        });

        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                finish();
            }
        });
    }

    /**
     * makes login request
     * @param userLoginRequest
     */
    private void userLogin(UserLoginRequest userLoginRequest){
        String url = BASE_URL + getString(R.string.login_url);

        Gson gson = new GsonBuilder().create();
        final String loginData = gson.toJson(userLoginRequest);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Login Response", response);
                LoginResponse loginResponse = new GsonBuilder().create().fromJson(response, LoginResponse.class);
                //saving login data in shared preference
                SharedPreferences loginPref = getSharedPreferences(getString(R.string.loginInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginPref.edit();
                editor.putInt(getString(R.string.loginId), loginResponse.getLogin().getLoginId().intValue());
                editor.putString(getString(R.string.token), loginResponse.getLogin().getToken());
                editor.apply();

                //saving user data in shared preference
                SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editUserPref = userPref.edit();
                editUserPref.putInt(getString(R.string.user_id), loginResponse.getLogin().getUserId().intValue());
                editUserPref.putString(getString(R.string.profile_picture), loginResponse.getLogin().getProfilePicture());
                editUserPref.putString(getString(R.string.full_name), loginResponse.getLogin().getFullName());
                editUserPref.putString(getString(R.string.username), loginResponse.getLogin().getUsername());
                editUserPref.putString(getString(R.string.email), loginResponse.getLogin().getEmail());
                editUserPref.apply();

                //saving notification service on
                SharedPreferences notiPreferences = getSharedPreferences(getString(R.string.notificationInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editNoti = notiPreferences.edit();
                editNoti.putBoolean("status", true);
                editNoti.apply();
                Toast.makeText(LoginActivity.this, "Login Successful!", Toast.LENGTH_SHORT).show();

                startActivity( new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return loginData.getBytes();
            }
            public String getBodyContentType(){
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
    }

    /**
     * validates login data
     * @return boolean
     */
    private Boolean validateLogin(){
        String mail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        Validator validator = new Validator();
        //check if email field is empty
        if(mail.isEmpty()){
            validator.warnField(email, "Email is Empty!");
            return false;
        }
        //check if password field is empty
        if(pass.isEmpty()){
            validator.warnField(password, "Password is Empty!");
            return false;
        }
        //check if email entered is valid
        if(!validator.validateEmailPattern(mail)){
            validator.warnField(email,"Enter valid Email!");
            return false;
        }
        return true;
    }
}
