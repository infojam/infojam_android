package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.adaptors.ContributionAdaptor;
import com.sevensemesterproject.infojam.dto.contribution.ContributionResponse;
import com.sevensemesterproject.infojam.dto.contribution.Datum;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContributionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String BASE_URL;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private long backPressedTime;
    private Toast backToast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contribution);
        BASE_URL = getString(R.string.base_url);
        if(!new SharedPreferencesUtil().checkLogin(this)){
            Toast.makeText(this, "Please Login!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }else
            getContributionData();


        navigationView = findViewById(R.id.nav_drawer_home);
        navigationView.setNavigationItemSelectedListener(this);

        View headView = navigationView.getHeaderView(0);
        ImageView userImg = headView.findViewById(R.id.nav_image);
        TextView fullnameView, emailView;
        fullnameView = headView.findViewById(R.id.nav_fullname);
        emailView = headView.findViewById(R.id.nav_email);
        SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo),Context.MODE_PRIVATE);
        String picUrl = userPref.getString(getString(R.string.profile_picture), "");
        if(!picUrl.isEmpty()){
            try{
                Picasso.get().load(picUrl)
                        .fit().into(userImg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        fullnameView.setText(userPref.getString(getString(R.string.full_name), ""));
        emailView.setText(userPref.getString(getString(R.string.email), ""));

        drawerLayout = findViewById(R.id.drawer);

        ImageView open = findViewById(R.id.drawer_open);

        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

    }

    /**
     * creating a reqeust to get my all contributions
     */
    private void getContributionData() {
        StringRequest getContReqeust = new StringRequest(Request.Method.GET,
                BASE_URL+getString(R.string.my_contribution_url),
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ContributionResponse contributionResponse = new GsonBuilder().create()
                        .fromJson(response, ContributionResponse.class);
                List<Datum> data = contributionResponse.getData();
                //declaring recycler view to store and display contribution data
                RecyclerView contributionList = findViewById(R.id.contributionList);
                contributionList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                contributionList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                contributionList.setAdapter(new ContributionAdaptor(data));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(sharedPreferences.getInt("id", 0)));
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(getContReqeust);
    }

    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            return;
        }else{
            backToast = Toast.makeText(getBaseContext(),"Press back again to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_home:
                Intent h = new Intent(ContributionActivity.this, HomeActivity.class);
                startActivity(h);
                finish();
                break;
            case R.id.menu_profile:
                Intent p = new Intent(ContributionActivity.this, ProfileActivity.class);
                startActivity(p);
                finish();
                break;
            case R.id.menu_report:
                Intent report = new Intent(ContributionActivity.this, ReportActivity.class);
                startActivity(report);
                finish();
                break;
            case R.id.menu_contribution:
                Intent contribution = new Intent(ContributionActivity.this, ContributionActivity.class);
                startActivity(contribution);
                finish();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(ContributionActivity.this, SettingsActivity.class);
                startActivity(settings);
                finish();
                break;
            case R.id.menu_help:
                Intent help = new Intent(ContributionActivity.this, HelpActivity.class);
                startActivity(help);
                finish();
                break;
            case R.id.menu_feed:
                Intent feed = new Intent ( ContributionActivity.this, FeedbackActivity.class);
                startActivity(feed);
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
