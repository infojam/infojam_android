
package com.sevensemesterproject.infojam.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditUserResponse {

    @SerializedName("user")
    @Expose
    private UserEdited userEdited;

    public void setUserEdited(UserEdited userEdited) {
        this.userEdited = userEdited;
    }

    public UserEdited getUserEdited() {
        return userEdited;
    }
}
