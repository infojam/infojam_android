
package com.sevensemesterproject.infojam.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotiContributionRequest {

    @SerializedName("contributionEnum")
    @Expose
    private String contributionEnum;

    public String getContributionEnum() {
        return contributionEnum;
    }

    public void setContributionEnum(String contributionEnum) {
        this.contributionEnum = contributionEnum;
    }

}
