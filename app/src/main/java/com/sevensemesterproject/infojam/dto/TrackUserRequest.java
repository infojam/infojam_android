
package com.sevensemesterproject.infojam.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackUserRequest {

    @SerializedName("lalitude")
    @Expose
    private Double lalitude;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("longitude")
    @Expose
    private Double longitude;

    public Double getLalitude() {
        return lalitude;
    }

    public void setLalitude(Double lalitude) {
        this.lalitude = lalitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
