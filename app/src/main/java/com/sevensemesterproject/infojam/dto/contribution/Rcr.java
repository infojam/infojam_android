
package com.sevensemesterproject.infojam.dto.contribution;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rcr {

    @SerializedName("contributionType")
    @Expose
    private String contributionType;
    @SerializedName("reportId")
    @Expose
    private Integer reportId;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;

    public String getContributionType() {
        return contributionType;
    }

    public void setContributionType(String contributionType) {
        this.contributionType = contributionType;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
