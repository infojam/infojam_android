
package com.sevensemesterproject.infojam.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("reporterId")
    @Expose
    private Integer reporterId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("lalitude")
    @Expose
    private Double lalitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("reportStatus")
    @Expose
    private String reportStatus;
    @SerializedName("jamStatus")
    @Expose
    private String jamStatus;
    @SerializedName("reporter")
    @Expose
    private String reporter;
    @SerializedName("contributiuonCount")
    @Expose
    private Integer contributiuonCount;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("createdTime")
    @Expose
    private String createdTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReporterId() {
        return reporterId;
    }

    public void setReporterId(Integer reporterId) {
        this.reporterId = reporterId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLalitude() {
        return lalitude;
    }

    public void setLalitude(Double lalitude) {
        this.lalitude = lalitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(String reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String getJamStatus() {
        return jamStatus;
    }

    public void setJamStatus(String jamStatus) {
        this.jamStatus = jamStatus;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public Integer getContributiuonCount() {
        return contributiuonCount;
    }

    public void setContributiuonCount(Integer contributiuonCount) {
        this.contributiuonCount = contributiuonCount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
