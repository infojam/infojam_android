
package com.sevensemesterproject.infojam.dto.contribution;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("rcr")
    @Expose
    private List<Rcr> rcr = null;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<Rcr> getRcr() {
        return rcr;
    }

    public void setRcr(List<Rcr> rcr) {
        this.rcr = rcr;
    }

}
