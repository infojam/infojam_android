package com.sevensemesterproject.infojam.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserReport {

    @SerializedName("jamstatus")
    @Expose
    private String jamstatus;

    public String getJamstatus() {
        return jamstatus;
    }

    public void setJamstatus(String jamstatus) {
        this.jamstatus = jamstatus;
    }

}