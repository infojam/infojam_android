
package com.sevensemesterproject.infojam.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActiveReportList {

    @SerializedName("data")
    @Expose
    private List<ReportData> data = null;

    public List<ReportData> getData() {
        return data;
    }

    public void setData(List<ReportData> data) {
        this.data = data;
    }

}
