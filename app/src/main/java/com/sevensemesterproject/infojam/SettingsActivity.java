package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.ChangePictureResponse;
import com.sevensemesterproject.infojam.dto.ChangeProfilePictureReqeust;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity {
    private TextView editProfile, changePassword, changeEmail, logout;
    private Switch switchNoti;
    private String BASE_URL;
    private ImageView changepic;
    private Button uploadpic;
    private ImageView cancleIcon;
    private static final int RESULT_LOAD_IMAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        BASE_URL = getString(R.string.base_url);
        if(!new SharedPreferencesUtil().checkLogin(this)){
            Toast.makeText(this, "Please Login!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }else
            setProfilePicture();
        changepic = findViewById(R.id.nav_image);
        changepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opens gallery when changepic imageview is clicked
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                uploadpic.setVisibility(View.VISIBLE);
                cancleIcon.setVisibility(View.VISIBLE);
            }
        });

        editProfile = findViewById(R.id.edit_profile);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, EditProfileActivity.class));
            }
        });

        changePassword = findViewById(R.id.change_password);
        changePassword.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ChangePasswordActivity.class));
                finish();
            }
        }));

        changeEmail = findViewById(R.id.change_email);
        changeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ChangeEmailActivity.class));
                finish();
            }
        });

        switchNoti = findViewById(R.id.switch_noti);
        switchNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.notificationInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(getString(R.string.noti_status), switchNoti.isChecked());
                editor.apply();//saves notification status in shared preference if changed
            }
        });
        loadNotiData();
        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.loginInfo), Context.MODE_PRIVATE);
                doLogout(sharedPreferences.getInt(getString(R.string.loginId), 0));
            }
        });

        uploadpic = findViewById(R.id.uploadpic);
        uploadpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap image = ((BitmapDrawable) changepic.getDrawable()).getBitmap();//change image picked to Bitmap
                doUpLoad(image);
                uploadpic.setVisibility(View.GONE);
                cancleIcon.setVisibility(View.GONE);
            }
        });
        cancleIcon = findViewById(R.id.cancle_icon);
        cancleIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setProfilePicture();
                uploadpic.setVisibility(View.GONE);
                cancleIcon.setVisibility(View.GONE);
            }
        });

    }

    /**
     * Sets profile picture in the circular imageView
     */
    private void setProfilePicture() {
        changepic = findViewById(R.id.nav_image);
        String picUrl = new SharedPreferencesUtil().getProfilePicture(this);
        Log.d("pp url", picUrl);
        if(!picUrl.isEmpty()){
            try{
                Picasso.get().load(picUrl)
                        .fit().into(changepic);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SettingsActivity.this, HomeActivity.class));
        finish();
    }

    /**
     * creates request for profile picture uploading
     * @param image
     */
    private void doUpLoad(Bitmap image) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);//encoding image to string
        Toast.makeText(this, "Uploading image...", Toast.LENGTH_SHORT).show();

        ChangeProfilePictureReqeust changeProfilePictureReqeust = new ChangeProfilePictureReqeust();
        changeProfilePictureReqeust.setProfilePicture(encodedImage);

        final String reqeustData = new GsonBuilder().create().toJson(changeProfilePictureReqeust);

        StringRequest uploadRequest = new StringRequest(Request.Method.PUT,
                BASE_URL+getString(R.string.change_ppic_url), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ChangePictureResponse picResponse = new GsonBuilder().create().fromJson(response, ChangePictureResponse.class);
                Toast.makeText(SettingsActivity.this, "Profile Picture Changed!", Toast.LENGTH_SHORT).show();
                //update profile picture url stored in shared preference
                SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = userPref.edit();
                editor.putString(getString(R.string.profile_picture), picResponse.getUrl());
                editor.apply();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences loginPref = getSharedPreferences(getString(R.string.loginInfo), Context.MODE_PRIVATE);
                SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(loginPref.getInt("id", 0)));
                params.put("id", String.valueOf(userPref.getInt("userId", 0)));
                params.put("token", loginPref.getString("token", ""));
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return reqeustData.getBytes();
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(uploadRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri selectImage = data.getData();
            changepic.setImageURI(selectImage);
        }
    }


    /**
     * loggin out logic
     * @param loginId
     */
    private void doLogout(int loginId) {
        String url = BASE_URL + "api/acc/" + loginId + "/logout";
        //creating logout request and handling response or error received
        StringRequest logoutReqeust = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(SettingsActivity.this, "Logged Out!", Toast.LENGTH_SHORT).show();
                clearPreferenceData();//clears data stored in shared preference after successful log out
                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(logoutReqeust);
    }

    /**
     * clear login and user data from shared preference after logout
     */
    private void clearPreferenceData() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.loginInfo), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.token), "");
        editor.putInt(getString(R.string.loginId), 0);
        editor.apply();

        SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
        SharedPreferences.Editor editUser = userPref.edit();
        editUser.putString(getString(R.string.username), "");
        editUser.putString(getString(R.string.full_name), "");
        editUser.putInt(getString(R.string.user_id), 0);
        editUser.putString(getString(R.string.profile_picture), "");
        editUser.apply();

        startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
        finish();
    }

    /**
     * get notification status stored in shared preference
     */
    private void loadNotiData() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.notificationInfo), Context.MODE_PRIVATE);
        Boolean status = sharedPreferences.getBoolean("status", false);
        switchNoti.setChecked(status);
    }

}
