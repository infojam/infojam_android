package com.sevensemesterproject.infojam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.UserSignupRequest;
import com.sevensemesterproject.infojam.utils.Validator;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

public class SignUpActivity extends AppCompatActivity {

    private EditText fullname,email,password,confirmpassword;
    private Button signupbtn;
    private TextView loginbtn;
    private String BASE_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        BASE_URL = getString(R.string.base_url);

        fullname = findViewById(R.id.signup_fullname);
        email = findViewById(R.id.signup_email);
        password = findViewById(R.id.signup_password);
        confirmpassword = findViewById(R.id.signup_confirmpassword);
        signupbtn = findViewById(R.id.signup_btn);
        loginbtn = findViewById(R.id.signup_login);

        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean isValid = validate();
                //sets data for sign up request only after validation
                if(isValid){
                    UserSignupRequest userSignupRequest = new UserSignupRequest();
                    userSignupRequest.setConfirmPassword(confirmpassword.getText().toString().trim());
                    userSignupRequest.setEmail(email.getText().toString().trim());
                    userSignupRequest.setFullName(fullname.getText().toString().trim());
                    userSignupRequest.setPassword(password.getText().toString().trim());
                    userSignup(userSignupRequest);
                }
            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    /**
     * sets data for sign up request
     * @param userSignupRequest
     */
    private void userSignup(UserSignupRequest userSignupRequest){
        String url = BASE_URL + getString(R.string.signup_url);
        Log.i("URL", url);
        Gson gson = new GsonBuilder().create();
        final String signupData = gson.toJson(userSignupRequest);
        Log.i("data: ", signupData);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(SignUpActivity.this, "Signup Completed!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return signupData.getBytes();
            }
            @Override
            public String getBodyContentType() {
                Log.d("content type", "application/json");
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
    }

    /**
     * validates signup data
     * @return boolean
     */
    private Boolean validate() {
        String conPw = confirmpassword.getText().toString().trim();
        String mail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        String fName = fullname.getText().toString().trim();
        Validator validator = new Validator();
        //check if fullname entered is empty
        if(fName.isEmpty()){
            validator.warnField(fullname, "Full Name can't be empty!");
            return false;
        }
        //check if email entered is empty
        if(mail.isEmpty()){
            validator.warnField(email, "Email can't be empty!");
            return false;
        }
        //check if password entered is empty
        if(pass.isEmpty()){
            validator.warnField(password, "Password can't be empty!");
            return false;
        }
        //check if confirm password entered is empty
        if (conPw.isEmpty()){
            validator.warnField(confirmpassword,"Confirm Password can't be empty!");
            return false;
        }
        //check if email entered is valid
        if(!validator.validateEmailPattern(mail)){
            email.requestFocus();
            email.setError("Email not valid!");
            return false;
        }
        //check if password entered is valid
        if(!validator.validatePasswordPattern(pass)){
            validator.warnField(password, "Password length must be at least 4 and should not have white space!");
            return false;
        }
        //check if password and confirmed password is same
        if(!validator.matchTwoPasswords(conPw, pass)){
            validator.warnField(password, "Password didn't match!");
            validator.warnField(confirmpassword,"Password didn't match!");
            return false;
        }
        return true;
    }
}
