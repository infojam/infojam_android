package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.ChangePasswordRequest;
import com.sevensemesterproject.infojam.utils.Validator;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.util.HashMap;
import java.util.Map;


public class ChangePasswordActivity extends AppCompatActivity {
    private EditText oldp, newp, confirmp;
    private Button changepasswordbtn;
    private String BASE_URL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        BASE_URL = getString(R.string.base_url);

        oldp = findViewById(R.id.oldp);
        newp = findViewById(R.id.newp);
        confirmp = findViewById(R.id.confirmp);

        changepasswordbtn = findViewById(R.id.change_password_button);
        changepasswordbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sets data for changing password only after validation
                if(validate()){
                    ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                    changePasswordRequest.setOldPassword(oldp.getText().toString());
                    changePasswordRequest.setNewPassword(newp.getText().toString());
                    changePasswordRequest.setConfirmNewPassword(confirmp.getText().toString());
                    changePassword(changePasswordRequest);
                }
            }
        });
    }

    /**
     * validates data for changing password
     * @return boolean
     */
    private boolean validate() {
        String conNewPass = confirmp.getText().toString().trim();
        String newPass = newp.getText().toString().trim();
        String oldPass = oldp.getText().toString().trim();
        Validator validator = new Validator();
        //check if old password entered is empty
        if(oldPass.isEmpty()){
            validator.warnField(oldp, "Old Password can't be empty!");
            return false;
        }
        //check if new password entered is empty
        if (newPass.isEmpty()){
            validator.warnField(newp,"New Password can't be empty!");
            return false;
        }
        //check if new password entered is valid
        if(!validator.validatePasswordPattern(newPass)){
            validator.warnField(newp, "Password length must be at least 4 and should not have white space!");
            return false;
        }
        //check if confirmed new password entered is empty
        if (conNewPass.isEmpty()){
            validator.warnField(confirmp,"Confirm Password can't be empty!");
            return false;
        }
        //check if new password and confirmed new password is same
        if(!validator.matchTwoPasswords(newPass, conNewPass)){
            validator.warnField(newp, "Password didn't match!");
            validator.warnField(confirmp,"Password didn't match!");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ChangePasswordActivity.this, SettingsActivity.class));
        finish();
    }

    /**
     * creates request for changing password
     * @param changePaswordRequest
     */
    private void changePassword(ChangePasswordRequest changePaswordRequest) {

        String url = BASE_URL + getString(R.string.change_password_url);

        Gson gson = new GsonBuilder().create();
        final String data = gson.toJson(changePaswordRequest);

        StringRequest putRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(ChangePasswordActivity.this, "Password Changed", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ChangePasswordActivity.this, SettingsActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return data.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String> params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(sharedPreferences.getInt("id", 0)));
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }

            public String getBodyContentType() {
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(putRequest);
    }
}
