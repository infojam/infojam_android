package com.sevensemesterproject.infojam;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.UserReport;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class ReportActivity extends FragmentActivity implements OnMapReadyCallback, OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private long backPressedTime;
    private Toast backToast;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    private Location userLocation;
    private Button report, submit;
    private String BASE_URL;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        BASE_URL = getString(R.string.base_url);
        if(!new SharedPreferencesUtil().checkLogin(this)){
            Toast.makeText(this, "Please Login!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();

        navigationView = findViewById(R.id.nav_drawer_home);
        navigationView.setNavigationItemSelectedListener(this);

        View headView = navigationView.getHeaderView(0);
        ImageView userImg = headView.findViewById(R.id.nav_image);
        TextView fullnameView, emailView;
        fullnameView = headView.findViewById(R.id.nav_fullname);
        emailView = headView.findViewById(R.id.nav_email);
        SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo),Context.MODE_PRIVATE);
        //setting data in navigation header
        String picUrl = userPref.getString(getString(R.string.profile_picture), "");
        if(!picUrl.isEmpty()){
            try{
                Picasso.get().load(picUrl)
                        .fit().into(userImg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        fullnameView.setText(userPref.getString(getString(R.string.full_name), ""));
        emailView.setText(userPref.getString(getString(R.string.email), ""));

        drawerLayout = findViewById(R.id.drawer);

        report = findViewById(R.id.report_btn);

        ImageView open = findViewById(R.id.drawer_open);

        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creates a dialog alert box when report button is clicked
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(ReportActivity.this);
                final View mView = getLayoutInflater().inflate(R.layout.activity_popup, null);
                submit = mView.findViewById(R.id.pop_report);
                radioGroup =(RadioGroup) mView.findViewById(R.id.radioGroup);
                Log.d("Radio Group Status", radioGroup.toString());
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        radioButton =(RadioButton) mView.findViewById(radioGroup.getCheckedRadioButtonId());
                        if(radioButton == null){
                            Toast.makeText(ReportActivity.this,"Enter Traffic density",Toast.LENGTH_SHORT).show();
                        }else{
                            String value = radioButton.getText().toString();
                            UserReport userReport = new UserReport();
                            userReport.setJamstatus(value);
                            userReport(userReport);
                            dialog.dismiss();
                        }
                    }
                });


            }
        });
    }

    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            return;
        }
        else{
            backToast = Toast.makeText(getBaseContext(),"Press back again to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    /**
     * creates request to post report
     * @param userReport
     */
    private void userReport(UserReport userReport){
        String url = BASE_URL + getString(R.string.post_report_url);
        final String data = new GsonBuilder().create().toJson(userReport);
        Log.d("Report Data Request", data);
        StringRequest postReport = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(ReportActivity.this, "Report Submitted!", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return data.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(sharedPreferences.getInt("id", 0)));
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }

            public String getBodyContentType(){
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postReport);
    }

    /**
     * fetches location
     */
    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            Log.d("Error", "Errors in permission");
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        Log.d("Task", task.toString());
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    userLocation = location;
                    Log.d("Location ", "lat>" + location.getLatitude() + ", lng>" + location.getLongitude());
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.map);
                    supportMapFragment.getMapAsync(ReportActivity.this);
                }
            }
        });
    }

    /**
     * create marker in map
     * @param googleMap
     */
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(false);
        //marking user location
        LatLng userLatLng = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        mMap.addMarker(new MarkerOptions().position(userLatLng)
                .title("My Location :" + userLocation.getLatitude() + ", " + userLocation.getLongitude())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 14F));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();


        switch (id) {
            case R.id.menu_home:
                Intent h = new Intent(ReportActivity.this, HomeActivity.class);
                startActivity(h);
                finish();

                break;
            case R.id.menu_profile:
                Intent p = new Intent(ReportActivity.this, ProfileActivity.class);
//                ProfileActivity.putExtra("uid",uid);
                startActivity(p);
                finish();

                break;

            case R.id.menu_report:
                Intent report = new Intent(ReportActivity.this, ReportActivity.class);
                startActivity(report);
                finish();

                break;
            case R.id.menu_contribution:
                Intent contribution = new Intent(ReportActivity.this, ContributionActivity.class);
                startActivity(contribution);
                finish();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(ReportActivity.this, SettingsActivity.class);
                startActivity(settings);
                finish();
                break;
            case R.id.menu_help:
                Intent help = new Intent(ReportActivity.this, HelpActivity.class);
                startActivity(help);
                finish();
                break;
            case R.id.menu_feed:
                Intent feed = new Intent(ReportActivity.this, FeedbackActivity.class);
                startActivity(feed);
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }
    /**
     * check login data stored in shared preference
     */
    private void checkLogin() {
        SharedPreferencesUtil util = new SharedPreferencesUtil();
        Boolean login = util.checkLogin(this);
        if (login == false) {
            Toast.makeText(this, "Please Login!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }
}
