package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.ProfileResponse;
import com.sevensemesterproject.infojam.dto.User;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView nv;
    private ImageView profilePic, editIcon;
    private long backPressedTime;
    private Toast backToast;
    private TextView name,email,dob,contact,address;
    private String BASE_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        BASE_URL = getString(R.string.base_url);

//        checkLogin();
        if(new SharedPreferencesUtil().checkLogin(ProfileActivity.this)){
            getUserDetails();
        }else {
            Toast.makeText(this, "Please Login!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        name = findViewById(R.id.prof_name);
        email = findViewById(R.id.prof_email);
        dob = findViewById(R.id.prof_dob);
        contact = findViewById(R.id.prof_cont);
        address = findViewById(R.id.prof_address);
        profilePic = findViewById(R.id.prof_img);

        drawerLayout = findViewById(R.id.drawerLayout);
        ImageView open = findViewById(R.id.drawer_open2);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        editIcon = findViewById(R.id.edit_icon);
        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), EditProfileActivity.class));
            }
        });
        nv = (NavigationView) findViewById(R.id.nav_drawer_profile);
        View headView = nv.getHeaderView(0);
        ImageView userImg = headView.findViewById(R.id.nav_image);
        TextView fullnameView, emailView;
        fullnameView = headView.findViewById(R.id.nav_fullname);
        emailView = headView.findViewById(R.id.nav_email);
        SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
        //setting data in navigation header
        String picUrl = userPref.getString(getString(R.string.profile_picture), "");
        if(!picUrl.isEmpty()){
            try{
                Picasso.get().load(picUrl)
                        .fit().into(userImg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        fullnameView.setText(userPref.getString(getString(R.string.full_name), ""));
        emailView.setText(userPref.getString(getString(R.string.email), ""));

        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.menu_home:
                        Intent h = new Intent(ProfileActivity.this, HomeActivity.class);
                        startActivity(h);
                        finish();

                        break;
                    case R.id.menu_profile:
                        Intent p = new Intent(ProfileActivity.this, ProfileActivity.class);
                        startActivity(p);
                        finish();
                        break;

                    case R.id.menu_report:
                        Intent report = new Intent(ProfileActivity.this, ReportActivity.class);
                        startActivity(report);
                        finish();

                        break;

                    case R.id.menu_feed:
                        Intent feed = new Intent(ProfileActivity.this, FeedbackActivity.class);
                        startActivity(feed);
                        finish();
                        break;

                    case R.id.menu_help:
                        Intent help = new Intent(ProfileActivity.this, HelpActivity.class);
                        startActivity(help);
                        finish();
                        break;

                    case R.id.menu_settings:
                        Intent settings = new Intent(ProfileActivity.this, SettingsActivity.class);
                        startActivity(settings);
                        finish();
                        break;

                    case R.id.menu_contribution:
                        Intent contribution = new Intent(ProfileActivity.this, ContributionActivity.class);
                        startActivity(contribution);
                        finish();
                        break;

                }
                return onContextItemSelected(item);

            }
        });


    }
    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            return;
        }
        else{
            backToast = Toast.makeText(getBaseContext(),"Press back again to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    /**
     * create request to get user details
     */
    private void getUserDetails() {
        int id = new SharedPreferencesUtil().getUserId(this);
        Log.d("Id", String.valueOf(id));
        String url = BASE_URL+getString(R.string.get_user_url)+id;
        StringRequest getRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                ProfileResponse profileResponse = new GsonBuilder().create().fromJson(response, ProfileResponse.class);
                User user = profileResponse.getUser();
                setUserDetails(user);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(getRequest);
    }

    /**
     * setting data into the views in profile activity
     * @param user
     */
    private void setUserDetails(User user) {
        if(user != null){
            if(!user.getProfilePicture().isEmpty()) {
                try{
                    Picasso.get().load(user.getProfilePicture())
                            .fit().into(profilePic);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
                address.setText(user.getAddress());
            /*else{
                address.setVisibility(View.GONE);
                ImageView addressIcon = findViewById(R.id.imageView_address);
                addressIcon.setVisibility(View.GONE);
                TextView addressText = findViewById(R.id.textView_address);
                addressText.setVisibility(View.GONE);
            }*/
                name.setText(user.getFullName());
                email.setText(user.getEmail());
                dob.setText(user.getDob());
                contact.setText(user.getPhone());
        }
        else
            Toast.makeText(this, "Something Went Wrong!", Toast.LENGTH_SHORT).show();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.menu_home:
                Toast.makeText(ProfileActivity.this, "HomeActivity", Toast.LENGTH_SHORT).show();
                Intent h = new Intent(ProfileActivity.this, HomeActivity.class);
                startActivity(h);
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(ProfileActivity.this, SettingsActivity.class);
                startActivity(settings);
                break;
        }
        return true;
    }


}
