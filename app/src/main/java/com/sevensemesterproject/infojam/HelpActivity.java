package com.sevensemesterproject.infojam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {
    private TextView question1, question2, question3, question4, question5, question6,
            answer1, answer2, answer3, answer4, answer5, answer6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
//        WebView webView = findViewById(R.id.help_web_view);
//        webView.setWebViewClient(new WebViewClient());
//        webView.loadUrl("http://www.google.com");

        question1 = findViewById(R.id.question1);
        question1.setText("1. What is InfoJam?");
        answer1 = findViewById(R.id.answer1);
        answer1.setText("It is an application that takes jam report across different users and broadcast that information among all other users.");
        question1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer1.getVisibility() == View.GONE)
                    answer1.setVisibility(View.VISIBLE);
                else
                    answer1.setVisibility(View.GONE);
            }
        });

        question2 = findViewById(R.id.question2);
        question2.setText("2. How do I get jam information?");
        answer2 = findViewById(R.id.answer2);
        answer2.setText("The active jam information are marked in map in Home. Jam with HIGH status are marked with Red, MEDIUM with orange and LOW with Orange. ");
        question2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer2.getVisibility() == View.GONE)
                    answer2.setVisibility(View.VISIBLE);
                else
                    answer2.setVisibility(View.GONE);
            }
        });

        question3 = findViewById(R.id.question3);
        question3.setText("3. How do I change my profile picture?");
        answer3 = findViewById(R.id.answer3);
        answer3.setText("Go to Settings> Click in the profile picture > Choose a new picture from gallary > click upload button.");
        question3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer3.getVisibility() == View.GONE)
                    answer3.setVisibility(View.VISIBLE);
                else
                    answer3.setVisibility(View.GONE);
            }
        });

        question4 = findViewById(R.id.question4);
        question4.setText("4. How do I change my password?");
        answer4 = findViewById(R.id.answer4);
        answer4.setText("Go to Settings> Click in the Change password option.");
        question4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer4.getVisibility() == View.GONE)
                    answer4.setVisibility(View.VISIBLE);
                else
                    answer4.setVisibility(View.GONE);
            }
        });

        question5 = findViewById(R.id.question5);
        question5.setText("5. How do I change my email?");
        answer5 = findViewById(R.id.answer5);
        answer5.setText("Go to Settings> Click in the Change email option.");
        question5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer5.getVisibility() == View.GONE)
                    answer5.setVisibility(View.VISIBLE);
                else
                    answer5.setVisibility(View.GONE);
            }
        });

        question6 = findViewById(R.id.question6);
        question6.setText("6. How do I log out?");
        answer6 = findViewById(R.id.answer6);
        answer6.setText("Go to Settings> Click in the Logout option.");
        question6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer6.getVisibility() == View.GONE)
                    answer6.setVisibility(View.VISIBLE);
                else
                    answer6.setVisibility(View.GONE);
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(HelpActivity.this, HomeActivity.class));
        finish();
    }
}

