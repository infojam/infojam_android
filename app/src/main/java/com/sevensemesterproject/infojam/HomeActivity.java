package com.sevensemesterproject.infojam;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.ActiveReportList;
import com.sevensemesterproject.infojam.dto.ReportData;
import com.sevensemesterproject.infojam.services.UserTrackingService;
import com.sevensemesterproject.infojam.services.notification.NotificationService;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private long backPressedTime;
    private Toast backToast;
    private Toolbar mainToolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private static final int REQUEST_CODE = 101;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location userLocation ;
    private String BASE_URL;
    private List<ReportData> reportDataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BASE_URL = getString(R.string.base_url);
        //fetching the location of the device
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();

        getReports();

        SharedPreferencesUtil sharedPreferencesUtil = new SharedPreferencesUtil();
        //checking login status of the user based on the shared preference stored
        boolean loginStatus = sharedPreferencesUtil.checkLogin(this);
        if(loginStatus){
            //if user login is available the only tracking service is invoked
            Intent intent = new Intent(this, UserTrackingService.class);
            startService(intent);
        }
        //checking notification status of the application and login status to start notification service
        if(sharedPreferencesUtil.getNotificationStatus(this) && loginStatus){
            Intent notiIntent = new Intent(this, NotificationService.class);
            startService(notiIntent);
        }

        navigationView = findViewById(R.id.nav_drawer_home);
        navigationView.setNavigationItemSelectedListener(this);

        View headView = navigationView.getHeaderView(0);
        ImageView userImg = headView.findViewById(R.id.nav_image);
        TextView fullnameView, emailView;
        fullnameView = headView.findViewById(R.id.nav_fullname);
        emailView = headView.findViewById(R.id.nav_email);
        //setting data in navigation header
        SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo),Context.MODE_PRIVATE);
        String picUrl = userPref.getString(getString(R.string.profile_picture), "");
        if(!picUrl.isEmpty() ){
            try{
                Picasso.get().load(picUrl)
                        .fit().into(userImg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        fullnameView.setText(userPref.getString(getString(R.string.full_name), ""));
        emailView.setText(userPref.getString(getString(R.string.email), ""));

        drawerLayout = findViewById(R.id.drawer);

        ImageView open = findViewById(R.id.drawer_open);

        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            return;
        }else{
            backToast = Toast.makeText(getBaseContext(),"Press back again to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(false);
        //setting marker for my location
        LatLng userLatLng = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        mMap.addMarker(new MarkerOptions().position(userLatLng).
                title("My Location :" + userLocation.getLatitude() + ", " + userLocation.getLongitude())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        //setting marker for the reports
        if(reportDataList != null){
            Log.d("Reports for marking>>", reportDataList.toString());
            Log.d("Count", String.valueOf(reportDataList.size()));
            for (ReportData data:reportDataList){
                Log.d("-----LatLng Data---", data.getLalitude()+", "+data.getLongitude()+": "+data.getLocation());
                if(data.getJamStatus().equals("LOW"))
                    mMap.addMarker(new MarkerOptions().position(new LatLng(data.getLalitude(), data.getLongitude()))
                            .title("Reported by: "+data.getReporter())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                else if(data.getJamStatus().equals("MEDIUM"))
                    mMap.addMarker(new MarkerOptions().position(new LatLng(data.getLalitude(), data.getLongitude()))
                            .title("Reported by: "+data.getReporter())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                else
                    mMap.addMarker(new MarkerOptions().position(new LatLng(data.getLalitude(), data.getLongitude()))
                            .title("Reported by: "+data.getReporter())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            }
        }else
            Toast.makeText(this, "No data available currently!", Toast.LENGTH_SHORT).show();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 14F));
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_home:
                Intent h = new Intent(HomeActivity.this, HomeActivity.class);
                startActivity(h);
                finish();

                break;
            case R.id.menu_profile:
                Intent p = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(p);
                finish();

                break;

            case R.id.menu_report:
                Intent report = new Intent(HomeActivity.this, ReportActivity.class);
                startActivity(report);
                finish();
                break;

            case R.id.menu_feed:
                Intent feed = new Intent ( HomeActivity.this, FeedbackActivity.class);
                startActivity(feed);
                finish();
                break;

            case R.id.menu_help:
                Intent help = new Intent(HomeActivity.this, HelpActivity.class);
                startActivity(help);
                finish();
                break;

            case R.id.menu_settings:
                Intent settings = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(settings);
                finish();
                break;

            case R.id.menu_contribution:
                Intent contribution = new Intent(HomeActivity.this, ContributionActivity.class);
                startActivity(contribution);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * fetches location
     */
    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            Log.d("Error", "Errors in permission");
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        Log.d("Task", task.toString());
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    userLocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.map);
                    supportMapFragment.getMapAsync(HomeActivity.this);
                }
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }

    /**
     * request reports from the server
     */
    private  void getReports() {
        final List<ReportData> data = new ArrayList<ReportData>();
        String url = BASE_URL + getString(R.string.get_all_report_url);
        Log.d("report url", url);
        StringRequest getReportsReqeust = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Report Response",response);
                ActiveReportList activeReportList = new GsonBuilder().create().fromJson(response, ActiveReportList.class);
                Log.d("Report Response",activeReportList.toString());
                Log.d("Report List Response", String.valueOf(activeReportList.getData().size()));
                setReportData(activeReportList.getData());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(getReportsReqeust);
    }

    /**
     * sets the data from server response in reportDataList
     * @param data
     */
    private void setReportData(List<ReportData> data) {

        if(data != null) {
            this.reportDataList = data;
        }else{
            Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
        }
    }

}
