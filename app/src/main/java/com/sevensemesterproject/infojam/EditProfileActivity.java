package com.sevensemesterproject.infojam;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.EditEmailResponse;
import com.sevensemesterproject.infojam.dto.EditProfileRequest;
import com.sevensemesterproject.infojam.dto.EditUserResponse;
import com.sevensemesterproject.infojam.dto.UserEdited;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.Validator;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {
    private TextView dob;
    private EditText address, fullname, phone;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private RadioGroup gender;
    private Button updateprofile;
    private RadioButton btn_gender;
    private String BASE_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        BASE_URL = getString(R.string.base_url);

        address = findViewById(R.id.edit_address);
        fullname = findViewById(R.id.edit_fullname);
//        fullname.setText(new SharedPreferencesUtil().getUserFullname(EditProfileActivity.this));
        phone = findViewById(R.id.edit_phone);
        gender =(RadioGroup) findViewById(R.id.gender_radio);
        dob = findViewById(R.id.edit_dob);
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                //creating date picker for date of birth field
                DatePickerDialog dialog = new DatePickerDialog(EditProfileActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth,
                        dateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        //formating the picked date and setting it in date of birth field
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month+1;
                String fm=""+month;
                String fd=""+day;
                if(month<10){
                    fm ="0"+month;
                }
                if (day<10){
                    fd="0"+day;
                }
                String date = year+"-"+fm+"-"+fd;
                Log.d("Date picked", date);
                dob.setText(date);

            }
        };

        updateprofile = findViewById(R.id.update_profile);
        updateprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValid = validate();
                //sets user editing data to create reqeust only if they are valid
                if(isValid){
                    btn_gender =(RadioButton) findViewById(gender.getCheckedRadioButtonId());
                    EditProfileRequest editProfileRequest = new EditProfileRequest();
                    editProfileRequest.setAddress(address.getText().toString().trim());
                    editProfileRequest.setFullName(fullname.getText().toString().trim());
                    editProfileRequest.setDob(dob.getText().toString().trim());
                    editProfileRequest.setPhone(phone.getText().toString().trim());
                    editProfileRequest.setGender(btn_gender.getText().toString().toUpperCase());
                    editProfile(editProfileRequest);
                }
            }
        });

    }

    /**
     * validates the edit user data
     * @return boolean
     */
    private boolean validate() {
        String add = address.getText().toString().trim();
        String fName = fullname.getText().toString().trim();
        String dBirth = dob.getText().toString();
        String ph = phone.getText().toString().trim();
        Validator validator = new Validator();
        //check if fullname entered is empty
        if(fName.isEmpty()){
            validator.warnField(fullname, "Fullname can't be Empty!");
            return false;
        }
        //check if phone entered is empty
        if(ph.isEmpty()){
            validator.warnField(phone, "Phone can't be Empty!");
            return false;
        }
        //check if phone entered is valid
        if (!validator.validatePhone(ph)) {
            validator.warnField(phone, "Enter valid Phone Number of 10 digits!");
            return false;
        }
        //check if dob entered is empty
        if(dBirth.isEmpty()){
            validator.warnField(dob, "Please enter Date of Birth!");
            return false;
        }
        //check if gender is selected or not
        if(gender.getCheckedRadioButtonId() == -1){
            Toast.makeText(this, "Select Gender!", Toast.LENGTH_SHORT).show();
            return false;
        }
        //check if address entered is empty
        if(add.isEmpty()){
            validator.warnField(address, "Please enter Address!");
            return false;
        }

        return true;
    }

    /**
     * creates edit request with valid data
     * @param editProfileRequest
     */
    private void editProfile(EditProfileRequest editProfileRequest) {
        String url = BASE_URL+getString(R.string.edit_user_url);
        Gson gson = new GsonBuilder().create();
        final String data = gson.toJson(editProfileRequest);

        StringRequest putRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                UserEdited editResponse = new GsonBuilder().create().fromJson(response, EditUserResponse.class).getUserEdited();
                Toast.makeText(EditProfileActivity.this, "Profile Edited!", Toast.LENGTH_SHORT).show();
                //updating data in shared preference after user data is edited
                SharedPreferences userPref = getSharedPreferences(getString(R.string.userInfo), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = userPref.edit();
                editor.putString(getString(R.string.full_name), editResponse.getFullName());
                editor.apply();

                startActivity(new Intent(EditProfileActivity.this, SettingsActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return data.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(getString(R.string.user_id), String.valueOf(new SharedPreferencesUtil().getUserId(EditProfileActivity.this)));
                params.put(getString(R.string.token), new SharedPreferencesUtil().getToken(EditProfileActivity.this));
                return params;
            }

            public String getBodyContentType() {
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(putRequest);

    }
}
