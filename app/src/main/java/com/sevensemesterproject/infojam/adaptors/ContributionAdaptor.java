package com.sevensemesterproject.infojam.adaptors;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.R;
import com.sevensemesterproject.infojam.dto.contribution.Datum;

import java.util.List;

public class ContributionAdaptor extends RecyclerView.Adapter<ContributionAdaptor.ContributionListHolder> {
    private List<Datum> data;
    public ContributionAdaptor(List<Datum> data){
        this.data = data;
        Log.d("Datum size", String.valueOf(data.size()));
    }
    @NonNull
    @Override
    public ContributionListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.contribution_list_layout, viewGroup, false);
        return new ContributionListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContributionListHolder viewHolder, int position) {
        Datum temp = data.get(position);

        viewHolder.date.setText(temp.getCreatedDate());

        LinearLayoutManager manager = new LinearLayoutManager(viewHolder.contributionRecyclerView.getContext());
        manager.setInitialPrefetchItemCount(temp.getRcr().size());

        viewHolder.contributionRecyclerView.setLayoutManager(manager);
        viewHolder.contributionRecyclerView.setAdapter(new ContributionDataAdaptor(temp.getRcr()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ContributionListHolder extends RecyclerView.ViewHolder{
        TextView date;
        RecyclerView contributionRecyclerView;
        public ContributionListHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.contribution_date);
            contributionRecyclerView = itemView.findViewById(R.id.contribution_data_recy_view);
        }
    }
}
