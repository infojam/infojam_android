package com.sevensemesterproject.infojam.adaptors;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.R;
import com.sevensemesterproject.infojam.dto.contribution.Rcr;

import java.util.List;

public class ContributionDataAdaptor extends RecyclerView.Adapter<ContributionDataAdaptor.ContributionDataHolder>{
    private List<Rcr> data;
    public ContributionDataAdaptor(List<Rcr> data){
        this.data = data;
        Log.d("RCR size", String.valueOf(data.size()));
    }
    @NonNull
    @Override
    public ContributionDataHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.contribution_data_layout,viewGroup, false);
        return new ContributionDataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContributionDataHolder dataHolder, int i) {
        Rcr rcr = data.get(i);
        Log.d("Contribution----", new GsonBuilder().create().toJson(rcr));
        if(rcr.getContributionType().equals("REPORTER")) {
            dataHolder.iconView.setImageResource(R.drawable.warning);
            dataHolder.dataView.setText(rcr.getCreatedBy()+", you reported a jam.");
        }
        else {
            dataHolder.iconView.setImageResource(R.drawable.success);
            dataHolder.dataView.setText("You said you were in a jam reported by "+ rcr.getCreatedBy());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ContributionDataHolder extends RecyclerView.ViewHolder {
        TextView dataView;
        ImageView iconView;
        public ContributionDataHolder(@NonNull View itemView) {
            super(itemView);
            dataView = itemView.findViewById(R.id.contribution_data_text);
            iconView = itemView.findViewById(R.id.contribution_icon);
        }
    }
}
