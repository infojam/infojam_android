package com.sevensemesterproject.infojam.services.notification;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.R;
import com.sevensemesterproject.infojam.dto.NotiContributionRequest;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.util.HashMap;
import java.util.Map;

public class NotificationReciever extends BroadcastReceiver {
    private String BASE_URL;
    private String dataAction;
    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        String action = intent.getAction();
        int notificatioId = intent.getIntExtra("id", -1);
        BASE_URL = context.getString(R.string.base_url);

        if(NotificationActionConstant.YES_ACTION.equals(action)) {
            dataAction = "YES";
        } else {
            dataAction = "NO";
        }
        NotiContributionRequest request = new NotiContributionRequest();
        request.setContributionEnum(dataAction);

        postContibution(request);

        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.cancel(notificatioId);
    }

    /**
     * created request for posting contribution
     * @param request
     */
    private void postContibution(final NotiContributionRequest request) {
        final String data = new GsonBuilder().create().toJson(request);

        StringRequest postRequest = new StringRequest(Request.Method.POST, BASE_URL + "api/contribution/sendContribution", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Contribution", "SUBMITTED");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).parseVolleyError(context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = context.getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(sharedPreferences.getInt("id", 0)));
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }
            public String getBodyContentType(){
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return data.getBytes();
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(postRequest);
    }
}
