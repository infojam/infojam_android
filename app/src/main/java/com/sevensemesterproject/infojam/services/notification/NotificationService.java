package com.sevensemesterproject.infojam.services.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.R;
import com.sevensemesterproject.infojam.dto.NotificationData;
import com.sevensemesterproject.infojam.dto.NotificationResponse;
import com.sevensemesterproject.infojam.utils.CheckNetworkStatus;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class NotificationService extends Service {
    private Timer timer;
    private String BASE_URL;
    private final String CHANNEL_ID = "report_notification";
    private final int NOTIFICATION_ID = 001;

    public NotificationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BASE_URL = getString(R.string.base_url);
        Log.d("Notification Service", "INITIATED!");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d("Notification Service", "RUNNING.......");
                getNotification();

                if(!checkLogin() || !getNotificationStatus() || !new CheckNetworkStatus().isNetworkAvailable(getApplicationContext())){
                    Log.d("Notification Service","SHUTTING DOWN........");
                    onDestroy();
                }
            }
        }, 0, 5000);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        Log.d("Notification Service", "STOPPING.......");
        super.onDestroy();
    }

    /**
     * check login status
     * @return boolean
     */
    private boolean checkLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token","");
        int id = sharedPreferences.getInt("id", 0);
        if(id == 0 && token == "")
            return false;
        else
            return true;
    }

    /**
     * check notification status
     * @return boolean
     */
    private boolean getNotificationStatus(){
        SharedPreferences sharedPreferences = getSharedPreferences("notification", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("status", false);
    }



    private void getNotification(){
        StringRequest notiRequest = new StringRequest(Request.Method.GET,
                BASE_URL + getString(R.string.get_my_notification_url), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                NotificationResponse notification = new GsonBuilder().create().fromJson(response, NotificationResponse.class);
                Log.d("Notification Response", response);
//                Log.d("Notification Data", notification.getNotificationData().toString());
                displayNotification(notification.getNotificationData());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).parseVolleyError(getApplicationContext());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences loginData = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String> param = new HashMap<String, String>();
                param.put("token",loginData.getString("token", ""));
                param.put("loginId", String.valueOf(loginData.getInt("id", 0)));
                return param;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(notiRequest);
    }

    public void displayNotification(NotificationData response) {
        createNotificationChannel();

        Intent yes = new Intent(this, NotificationReciever.class);
//        yes.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        yes.putExtra("id", NOTIFICATION_ID);
        yes.setAction(NotificationActionConstant.YES_ACTION);

        Intent no = new Intent(this, NotificationReciever.class);
//        no.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        no.putExtra("id", NOTIFICATION_ID);
        no.setAction(NotificationActionConstant.NO_ACTION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setOnlyAlertOnce(true);//alert only once for the same notification
        builder.setSmallIcon(R.drawable.infojam_logo);
        builder.setContentTitle("Jam Report");
        builder.setContentText("Someone reported jam near you. Do you agree?");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setAutoCancel(true);
        builder.addAction(new NotificationCompat.Action(
                R.drawable.yes,
                "Yes",
                PendingIntent.getBroadcast(this, 0, yes, PendingIntent.FLAG_ONE_SHOT)));
        builder.addAction(new NotificationCompat.Action(
                R.drawable.no,
                "No",
                PendingIntent.getBroadcast(this, 0, no, PendingIntent.FLAG_ONE_SHOT)));
        
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }
    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "personal Notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription("Notification for Oreo and above.");

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);

        }
    }
}
