package com.sevensemesterproject.infojam.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.R;
import com.sevensemesterproject.infojam.dto.TrackUserRequest;
import com.sevensemesterproject.infojam.utils.CheckNetworkStatus;
import com.sevensemesterproject.infojam.utils.GetLocationKeyAddress;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class UserTrackingService extends Service {
    private Timer timer;
    private FusedLocationProviderClient fusedLocationClient;
    private String BASE_URL;
    private GetLocationKeyAddress getLocationKeyAddress;


    public UserTrackingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
//         TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Log.d("Tracking service", "INITIATED!");
        super.onCreate();
        BASE_URL = getString(R.string.base_url);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer = new Timer();
        //Set the schedule function for tracking
        Log.d("Tracking Timer", "30000ms");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d("Tracking service", "RUNNING.......");
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                //gets location
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    Log.d("Location ", "lat>" + location.getLatitude() + ", lng>" + location.getLongitude());
                                    getLocationKeyAddress = new GetLocationKeyAddress(getApplicationContext());
                                    String address = null;
                                    try {
                                        address = getLocationKeyAddress.getKeyLocation(location.getLatitude(), location.getLongitude());
                                        Log.d("Tracked Address", address);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    //sets the found location to send to backend
                                    TrackUserRequest trackUserRequest = new TrackUserRequest();
                                    trackUserRequest.setLalitude(location.getLatitude());
                                    trackUserRequest.setLongitude(location.getLongitude());
                                    trackUserRequest.setLocation(address);
                                    trackUserReqeust(trackUserRequest);
                                }
                                else
                                    Log.d("Error", "Error in fetching location!");
                            }
                        });
                //if wifi/data is off or user has logged out, tracking service is stopped
                if(!new CheckNetworkStatus().isNetworkAvailable(getApplicationContext()) || !checkLogin()){
                    Log.d("Tracking Service","SHUTTING DOWN........");
                    onDestroy();
                }
            }
        }, 0, 30000);
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        timer.cancel();
        super.onDestroy();
        Log.d("TRACKING","SHUT DOWN!");
    }

    /**
     * check login for stopping track service
     * @return boolean
     */
    private boolean checkLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token","");
        int id = sharedPreferences.getInt("id", 0);
        if(id == 0 && token == "")
            return false;
        else
            return true;
    }

    /**
     * create PUT request to edit tracking data
     * @param trackUserRequest
     */
    private void trackUserReqeust(TrackUserRequest trackUserRequest){
        String url = BASE_URL+getString(R.string.track_me_url);
        final String trackData = new GsonBuilder().create().toJson(trackUserRequest);
        Log.d("Request Data", trackData);
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Track Response", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Tracking error", error.toString());
                new VollyErrorChecker(error).parseVolleyError(getApplicationContext());
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return trackData.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(sharedPreferences.getInt("id", 0)));
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }

            public String getBodyContentType(){
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(putRequest);
    }
}
