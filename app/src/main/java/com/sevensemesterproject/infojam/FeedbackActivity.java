package com.sevensemesterproject.infojam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.sevensemesterproject.infojam.dto.FeedbackRequest;
import com.sevensemesterproject.infojam.utils.SharedPreferencesUtil;
import com.sevensemesterproject.infojam.utils.VollyErrorChecker;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {

    private EditText title, description;
    private Button feedbtn;
    private String BASE_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        checkLogin();
        BASE_URL = getString(R.string.base_url);

        title = findViewById(R.id.feed_title);
        description = findViewById(R.id.feed_desc);

        feedbtn = findViewById(R.id.feed_btn);
        feedbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackRequest feedbackRequest = new FeedbackRequest();
                feedbackRequest.setTitle(title.getText().toString().trim());
                feedbackRequest.setDescription(description.getText().toString().trim());
                submitFeedback(feedbackRequest);
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FeedbackActivity.this, HomeActivity.class));
        finish();
    }

    /**
     * creates feedback submit request
     * @param feedbackRequest
     */
    private void submitFeedback(FeedbackRequest feedbackRequest) {
        final String data = new GsonBuilder().create().toJson(feedbackRequest);

        StringRequest postRequest = new StringRequest(Request.Method.POST, BASE_URL+getString(R.string.send_feedback_url),
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(FeedbackActivity.this, "Feedback Sent. Thank You!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(FeedbackActivity.this, HomeActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VollyErrorChecker(error).checkNetowrkError();
                new VollyErrorChecker(error).toastVolleyError(getApplicationContext());
            }
        }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return data.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.loginInfo), Context.MODE_PRIVATE);
                Map<String, String> params = new HashMap<String, String>();
                params.put("loginId", String.valueOf(sharedPreferences.getInt("id", 0)));
                params.put("token", sharedPreferences.getString("token", ""));
                return params;
            }

            public String getBodyContentType() {
                return "application/json";
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(postRequest);
    }

    /**
     * check login data stored in shared preference
     */
    private void checkLogin() {
        SharedPreferencesUtil util = new SharedPreferencesUtil();
        Boolean login = util.checkLogin(this);
        if (login == false) {
            Toast.makeText(this, "Please Login!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }
}
