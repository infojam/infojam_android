package com.sevensemesterproject.infojam.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.sevensemesterproject.infojam.EditProfileActivity;
import com.sevensemesterproject.infojam.R;

public class SharedPreferencesUtil {
    private static String token;
    private static int loginId;
    SharedPreferences sharedPreferences;

    /**
     * check if user login credentials are stored or not
     * @param activity
     * @return boolean
     */
    public Boolean checkLogin(Activity activity) {
        sharedPreferences = activity.getSharedPreferences(activity.getString(R.string.loginInfo), Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token","");
        loginId = sharedPreferences.getInt("id", 0);
        if(loginId == 0 && token == "")
            return false;
        else
            return  true;
    }

    /**
     * get login token stored in shared preference, if it is not stored it returns null
     * @param activity
     * @return String
     */
    public String getToken(Activity activity){
        sharedPreferences = activity.getSharedPreferences(activity.getString(R.string.loginInfo), Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        Log.d("token", token);
        return token;
    }

    /**
     *get login id stored in shared preference, if id is not stored it returns '0' as default value
     * @param activity
     * @return int
     */
    public int getLoginId(Activity activity){
        sharedPreferences = activity.getSharedPreferences(activity.getString(R.string.loginInfo), Context.MODE_PRIVATE);
        loginId = sharedPreferences.getInt("id", 0);
        return loginId;
    }

    /**
     * return notification status (on/off from settings)
     * @param activity
     * @return boolean
     */
    public boolean getNotificationStatus(Activity activity){
        sharedPreferences = activity.getSharedPreferences(activity.getString(R.string.notificationInfo), Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("status", false);
    }

    /**
     * get profile picture stored in shared preference
     * @param activity
     * @return url
     */
    public String getProfilePicture(Activity activity){
        SharedPreferences sharedPreferences = activity
                .getSharedPreferences(activity.getString(R.string.userInfo), Context.MODE_PRIVATE);
        return sharedPreferences.getString(activity.getString(R.string.profile_picture), "");
    }

    /**
     * get user id stored in shared preference
     * @param activity
     * @return id
     */
    public int getUserId(Activity activity){
        SharedPreferences sharedPreferences = activity
                .getSharedPreferences(activity.getString(R.string.userInfo), Context.MODE_PRIVATE);
        return sharedPreferences.getInt(activity.getString(R.string.user_id), 0);
    }

    public String getUserFullname(Activity activity) {
        SharedPreferences sharedPreferences = activity
                .getSharedPreferences(activity.getString(R.string.userInfo), Context.MODE_PRIVATE);
        return sharedPreferences.getString(activity.getString(R.string.full_name), "");
    }
}
