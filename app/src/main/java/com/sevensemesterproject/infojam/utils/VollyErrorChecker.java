package com.sevensemesterproject.infojam.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class VollyErrorChecker {
    private VolleyError error;

    /**
     * Parameterized Constructor
     * @param volleyError
     */
    public VollyErrorChecker(VolleyError volleyError){
        this.error = volleyError;
    }

    /**
     * Check network error
     */
    public void checkNetowrkError(){
        NetworkResponse response = error.networkResponse;
        if(error instanceof ServerError && response != null){
            try{
                String res = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                JSONObject obj = new JSONObject(res);
            }catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        Log.d("Network Error", error.toString());
    }

    /**
     * creates a toast with error message
     * @param context
     */
    public void toastVolleyError(Context context) {
        try {
            if(error.networkResponse == null) {
                Toast.makeText(context, "No Response From Server!", Toast.LENGTH_SHORT).show();
                return;
            }
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            String message = data.getString("message");
            int code = data.getInt("status");
            Log.d("Status", String.valueOf(code));
            Log.d("Error Message", message);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }

    /**
     * creates a log with error message
     * @param context
     */
    public void parseVolleyError(Context context) {
        try {
            if(error.networkResponse == null) {
                Log.d("ERROR!", "No Response From Server!");
                return;
            }
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            String message = data.getString("message");
            String error = data.getString("error");
            String trace = data.getString("trace");
            String timestamp = data.getString("timestamp");
            String path = data.getString("path");
            int code = data.getInt("status");
            //Logging the error attributes
            Log.d("Error", error);
            Log.d("Error TimeStamp", timestamp);
            Log.d("Status", String.valueOf(code));
            Log.d("Error Message", message);
//            Log.d("Error Trace", trace);
            Log.d("Error Path", path);
        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }

}
