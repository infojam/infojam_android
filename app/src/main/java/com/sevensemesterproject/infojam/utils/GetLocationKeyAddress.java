package com.sevensemesterproject.infojam.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GetLocationKeyAddress {
    private Context context;
    public GetLocationKeyAddress(Context context){
        this.context = context;
    }

    /**
     * Get address for a given location latitiude and longitude
     * @param latitude
     * @param longitude
     * @return address
     * @throws IOException
     */
    public String getKeyLocation(double latitude, double longitude) throws IOException {
        String keyLocation = null;
        String address = null, city = null, state = null, country = null, postalCode = null, knownName = null;
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {

                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        keyLocation = knownName+" ,"+city;
        return keyLocation;
    }
}
