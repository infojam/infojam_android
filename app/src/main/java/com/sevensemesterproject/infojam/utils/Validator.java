package com.sevensemesterproject.infojam.utils;

import android.widget.TextView;

import java.util.regex.Pattern;

public class Validator {
    /**
     * check if password is valid or not
     * @param password
     * @return boolean
     */
    public boolean validatePasswordPattern(String password){
        Pattern password_pattern  =
                Pattern.compile("^" +
                        //"(?=.*[0-9])" +         //at least 1 digit
                        //"(?=.*[a-z])" +         //at least 1 lower case letter
                        //"(?=.*[A-Z])" +         //at least 1 upper case letter
                        "(?=.*[a-zA-Z])" +      //any letter
                        //"(?=.*[@#$%^&+=])" +    //at least 1 special character
                        "(?=\\S+$)" +           //no white spaces
                        ".{4,}" +               //at least 4 characters
                        "$");
        return password_pattern.matcher(password).matches();
    }

    /**
     * check if email is valid or not
     * @param email
     * @return boolean
     */
    public boolean validateEmailPattern(String email){
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.compile(EMAIL_PATTERN).matcher(email).matches();
    }

    /**
     * check if phone is valid or not
     * @param phone
     * @return boolean
     */
    public boolean validatePhone(String phone){
        final String MOBILE_PATTERN = "^\\+?\\d{0,3}[-.\\s]?[9][87]\\d{8}?";
        return Pattern.compile(MOBILE_PATTERN).matcher(phone).matches();
    }

    /**
     * check if two passwords match or not
     * @param pw1
     * @param pw2
     * @return boolean
     */
    public boolean matchTwoPasswords(String pw1, String pw2){
        if(!pw1.equals(pw2)){
            return false;
        }else
            return true;
    }

    /**
     * check if two emails are same or not
     * @param email1
     * @param email2
     * @return
     */
    public boolean matchTwoEmails(String email1, String email2) {
        if(!email1.equals(email2)){
            return false;
        }else
            return true;
    }

    /**
     * for the given textview the error message is displayed
     * @param textView
     * @param message
     */
    public void warnField(TextView textView, String message){
        textView.requestFocus();
        textView.setError(message);
    }

}
